public class Task6 {
    public static void main(String[] args) {

        Product apple =new Product("apple", 50.50,5);
        Product tomatoes=new Product("tomatoes", 30, 6);
        Product cucumber=new Product("cucumber", 30, 6);

        Product toothbrush=new Product("colgate", 40, 4);
        Product floss=new Product("ROCKS", 300, 7);



        Product[] foodArray=new Product[]{apple, tomatoes, cucumber};
        Product[] toothArray=new Product[]{toothbrush, floss};


        Category categoryFood=new Category("Food", foodArray);
        Category categoryTooth=new Category("Tooth", toothArray);

        Product[] shoppingProducts=new Product[]{apple, floss, cucumber};

        Basket basket=new Basket(shoppingProducts);

        User user=new User(basket, "Ivan", "123ABC");



    }
}
