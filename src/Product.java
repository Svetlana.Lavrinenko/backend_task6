public class Product {

    private String productName;
    private double price;
    private int rating;



    public Product(String productName, double price, int rating) {
        this.productName = productName;
        this.price = price;
        this.rating = rating;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getLevel() {
        return rating;
    }

    public void setLevel(int rating) {
        this.rating = rating;
    }
}
