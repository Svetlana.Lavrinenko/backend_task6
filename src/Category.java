public class Category {


    private String categoryName;
    private Product[] products;

    public Category(String categoryName, Product[] products) {
        this.categoryName = categoryName;
        this.products = products;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Product[] getArrayString() {
        return products;
    }

    public void setArrayString(Product[] products) {
        this.products = products;
    }
}
